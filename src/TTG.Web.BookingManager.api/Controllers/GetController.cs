﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TTG.Web.BookingManager.Business;
using TTG.Web.Core.Common.Models;
using TTG.Web.Core.Common.Models.Http;

namespace TTG.Web.BookingManager.Api.Controllers
{
    [Route("BookingManager/[controller]")]
    [ApiController]
    public class GetController : ControllerBase
    {
        private readonly IBookingManager _bookingManager;

        public GetController(IBookingManager bookingManager)
        {
            _bookingManager = bookingManager;
        }

        [HttpGet]
        [Route("AllBookings")]
        public async Task<ActionResult<Response<Booking>>> AllBookings()
        {
            try
            {
                var result = await _bookingManager.GetAllBookingAsync();
                return Ok(result);
            }
            catch (Exception ex)
            {
                var result = new Response<bool>
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
                return Ok(result);
            }
        }

        [HttpGet]
        [Route("BookingById")]
        public async Task<ActionResult<Response<Booking>>> GetBookingById(int id)
        {
            try
            {
                var result = await _bookingManager.GetBookingAsync(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var result = new Response<bool>
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
                return Ok(result);
            }
        }
    }
}
