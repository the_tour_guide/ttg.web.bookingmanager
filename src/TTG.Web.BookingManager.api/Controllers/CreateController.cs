﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TTG.Web.BookingManager.Business;
using TTG.Web.Core.Common.Models;
using TTG.Web.Core.Common.Models.Http;

namespace TTG.Web.BookingManager.api.Controllers
{
    [Route("BookingManager/[controller]")]
    [ApiController]
    public class CreateController : ControllerBase
    {
        private readonly IBookingManager _bookingManager;
        public CreateController(IBookingManager bookingManager)
        {
            _bookingManager = bookingManager;
        }

        [HttpPost]
        [Route("Booking")]
        public async Task<ActionResult<Response<bool>>> AddBooking(Request<Booking> input)
        {
            try
            {
                Response<int> result = await _bookingManager.AddBookingAsync(input.Data);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var result = new Response<bool>
                {
                    IsSuccesful = false,
                    ErrorMessage= ex.Message
                };
                return Ok(result);
            }
        }
    }
}

