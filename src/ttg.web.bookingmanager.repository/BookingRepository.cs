﻿using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using TTG.Web.Core.Common.Models;
using TTG.Web.Core.Data.Repository;

namespace TTG.Web.BookingManager.Repository
{
    public class BookingRepository : RepoFunctions<Booking>, IBookingRepository
    {
        private readonly TTGDbContext _context;

        public BookingRepository(TTGDbContext context)
        {
            _context = context;
        }

        public async Task<List<Booking>> GetAllBookingAsync()
        {
            try
            {
                return await _context.Bookings.ToListAsync();
            }
            catch (Exception ex)
            {
                throw new Exception("Something broke when fetching all bookings", ex);
            }
        }

        public async Task<Booking> GetBookingAsync(int bookingId)
        {
            try
            {
                return await _context.Bookings.Where(booking => booking.BookingId == bookingId).FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                throw new Exception("Something broke when fetching all bookings", ex);
            }
        }

        public async Task<int> AddBookingAsync(Booking booking)
        {
            return await base.CreateAsync(booking);
        }

        public async Task<int> UpdateBookingAsync(Booking booking)
        {
            return await base.UpdateAsync(booking);
        }

        public async Task<int> DeleteBookingAsync(int bookingID)
        {
            return await base.DeleteAsync(new Booking { BookingId = bookingID });
        }
    }
}
