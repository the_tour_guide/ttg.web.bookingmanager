﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TTG.Web.BookingManager.Repository;
using TTG.Web.Core.Common.Models;
using TTG.Web.Core.Common.Models.Http;

namespace TTG.Web.BookingManager.Business
{
    public class BookingManager : IBookingManager
    {
        private readonly IBookingRepository _bookingRepository;

        public BookingManager(IBookingRepository bookingRepository)
        {
            _bookingRepository = bookingRepository;
        }

        public async Task<Response<int>> AddBookingAsync(Booking booking)
        {
            try
            {
                var result = await _bookingRepository.AddBookingAsync(booking);
                return new Response<int>
                {
                    IsSuccesful = true,
                    Data = result
                };
            }
            catch (Exception ex)
            {
                return new Response<int>
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
            }
        }
        public async Task<Response> DeleteBookingAsync(int bookingId)
        {
            try
            {
                var result = await _bookingRepository.DeleteBookingAsync(bookingId);
                return new Response
                {
                    IsSuccesful = true
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        public async Task<Response> GetAllBookingAsync()
        {
            try
            {
                var result = await _bookingRepository.GetAllBookingAsync();
                return new Response<IList<Booking>>
                {
                    IsSuccesful = true,
                    Data = result
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        public async Task<Response> GetBookingAsync(int Id)
        {
            try
            {
                var result = await _bookingRepository.GetBookingAsync(Id);
                return new Response
                {
                    IsSuccesful = true,
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        public async Task<Response> UpdateBookingAsync(Booking booking)
        {
            try
            {
                var result = await _bookingRepository.UpdateBookingAsync(booking);
                return new Response<int>
                {
                    IsSuccesful = true,
                    Data = result
                };
            }
            catch (Exception ex)
            {
                return new Response<int>
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
            }
        }
    }
}
