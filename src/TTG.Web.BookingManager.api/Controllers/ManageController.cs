﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TTG.Web.BookingManager.Business;
using TTG.Web.Core.Common.Models;
using TTG.Web.Core.Common.Models.Http;

namespace TTG.Web.BookingManager.Api.Controllers
{
    [Route("BookingManager/[controller]")]
    [ApiController]
    public class ManageController : ControllerBase
    {
        private readonly IBookingManager _bookingManager;
        public ManageController(IBookingManager bookingManager)
        {
            _bookingManager = bookingManager;
        }

        [HttpPut]
        [Route("UpdateBooking")]
        public async Task<ActionResult<Response<Booking>>> UpdateBooking(Request<Booking> input)
        {
            try
            {
                var result = await _bookingManager.UpdateBookingAsync(input.Data);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var result = new Response<bool>
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
                return Ok(result);
            }
        }

        [HttpDelete]
        [Route("DeleteBooking")]
        public async Task<ActionResult<Response<Booking>>> DeleteBooking(Request input)
        {
            try
            {
                var result = await _bookingManager.DeleteBookingAsync(input.UserID);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var result = new Response<bool>
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
                return Ok(result);
            }
        }
    }
}
