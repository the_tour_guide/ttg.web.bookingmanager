﻿using System.Linq.Expressions;
using TTG.Web.Core.Common.Models;

namespace TTG.Web.BookingManager.Repository
{
    public interface IBookingRepository
    {
        Task<int> AddBookingAsync(Booking booking);
        Task<List<Booking>> GetAllBookingAsync();
        Task<Booking> GetBookingAsync(int bookingId);
        Task<int> UpdateBookingAsync(Booking booking);
        Task<int> DeleteBookingAsync(int bookingId);
    }
}
