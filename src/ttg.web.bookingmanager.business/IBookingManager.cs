﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TTG.Web.Core.Common.Models;
using TTG.Web.Core.Common.Models.Http;

namespace TTG.Web.BookingManager.Business
{
    public interface IBookingManager
    {
        Task<Response> GetAllBookingAsync();
        Task<Response> GetBookingAsync(int id);
        Task<Response<int>> AddBookingAsync(Booking booking);
        Task<Response> UpdateBookingAsync(Booking bookingId);
        Task<Response> DeleteBookingAsync(int bookingId);

    }
}
